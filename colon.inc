%define dict_head 0

%macro colon 2
    %ifstr %1
        %ifid %2
            %2: dq dict_head
            db %1, 0
            %define dict_head %2
        %else
            %error "Second parameter should be an identifier"
        %endif
    %else
        %error "First parameter (key) should be a string"
    %endif
%endmacro
