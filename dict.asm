section .text

extern string_equals

; -> rdi - key to search for
; -> rsi - dict head
; <- rax - 1 if entry found 0 if not
global find_word
find_word:
.loop:
	push rsi
	push rdi
	add rsi, 8

	call string_equals

	pop rdi
	pop rsi

	test rax, rax
	jnz .entry_found

	mov rsi, [rsi]
	test rsi, rsi
	jnz .loop

.entry_found:
	mov rax, rsi
	ret
