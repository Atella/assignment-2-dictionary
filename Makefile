TARGET = dict

OBJ_FILES = $(patsubst %.asm, %.o, $(wildcard *.asm))

all: $(TARGET)

$(TARGET): $(OBJ_FILES)
	ld -o $(TARGET) $(OBJ_FILES)

%.o : %.asm
	nasm -g -felf64 $<

clean:
	rm *.o $(TARGET)