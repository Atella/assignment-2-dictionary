%include "words.inc"
%include "lib.inc"

%define BUF_SIZE 256

section .rodata

not_found_message: db "No value for requested key is found", 0xA, 0
bad_input_message: db "Input key is too long (max length : 255)", 0xA, 0

section .bss

input_buffer: resb BUF_SIZE 

section .text

global _start
_start:
.read_loop:
	push rcx	; rcx - current char count
	call read_char
	pop rcx

	test al, al ; if '\n' or str terminator was read -> str ended
	je .loop_end
	cmp al, `\n`
	je .loop_end

	mov [input_buffer + rcx], al	; save char in stack
	inc rcx

	cmp rcx, BUF_SIZE
	jne .read_loop

	mov rdi, bad_input_message
	call print_string

	mov rdi, 1
	call exit

.loop_end:
	mov [input_buffer + rcx], byte 0	; Add string terminator
	mov rsi, dict_head
	mov rdi, input_buffer

	call find_word

	test rax, rax
	jz .no_entry_found

	add rax, 8				; move rax from next entry pointer to key
	push rax

	mov rdi, rax
	call string_length		; find key length

	pop rdi					; move rdi to key pointer
	add rdi, rax			; move rax to value pointer
	inc rdi 				; +1 because of str terminator

	call print_string
	call print_newline

	mov rdi, 0
	call exit

.no_entry_found:
	mov rdi, not_found_message
	call print_string

	mov rdi, 0
	call exit